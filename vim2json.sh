#!/bin/bash
if [ "$1z" == "jsz" ]; then
    PREFIX="const conditions = "
    POSTFIX="export default conditions"
    REGEX_START="/("
    REGEX_END=")/mg"
else
    PREFIX=""
    POSTFIX=""
    REGEX_START='"'
    REGEX_END='"'
fi;
grep ^syn | grep match | sed -e's/^.*match\s*//g' | sed -e's/\\\([\\+(|)].\)/\1/g' | grep -v ^oldValue | sed -e's/".*//g' | sed -e's/contains.*//g' | sed -e"s:^\([^ ]*\)\s*/\([^/]*\).*$:  [\"\1\", $REGEX_START\2$REGEX_END],__EOL__:g" | tr -d '\n' | sed -e"s/^\(.*\),__EOL__/$PREFIX[__EOL__\1__EOL__]__EOL__$POSTFIX __EOL__/g" | sed -e"s/__EOL__/\n/g"

