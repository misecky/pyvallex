from pathlib import Path

from pytest import mark  # type: ignore

from vallex import load_file

BENCH_DATA = (Path(__file__).parent.parent/'data'/'verbs-bench.txt')


def bench_parser():
    with BENCH_DATA.open(encoding='utf-8') as IO:
        return load_file(IO)


@mark.skip_gitlab
def test_parse_verbs(collect_runs):
    collect_runs(10, bench_parser)
