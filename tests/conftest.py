import os


def pytest_configure():
    os.environ['_PYTEST_RUN'] = '1'


def pytest_unconfigure():
    if '_PYTEST_RUN' in os.environ:
        del os.environ['_PYTEST_RUN']
