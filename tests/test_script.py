from configparser import ConfigParser
from pathlib import Path

from vallex import add_file_to_collection, LexiconCollection
from vallex.scripts import SCRIPTS, load_script_file, run_script, run_scripts, prepare_requirements, RES_ERROR, RES_FAIL, RES_PASS, RES_SKIP
from vallex.scripts.utils import _REQUIREMENTS


def setup():
    SCRIPTS.clear()
    _REQUIREMENTS.clear()


def test_invalid_script():
    scrs = load_script_file(Path(__file__).parent/'data'/'scripts'/'invalid_script')
    assert not scrs


def test_valid_script():
    coll = LexiconCollection()
    cfg = ConfigParser()
    add_file_to_collection(coll, (Path(__file__).parent/'data'/'verbs.txt').open('r', encoding='utf-8'))

    scripts = load_script_file(Path(__file__).parent/'data'/'scripts'/'scripts.py')
    assert len(scripts) == 5

    prepare_requirements(cfg, coll)

    tst_scripts = [s for s in scripts if s.__name in ['fake_test', 'error']]
    assert len(tst_scripts) == 2

    for script in tst_scripts:
        res, msg = run_script(coll, script)
        if script.__name == 'fake_test':
            assert res == RES_PASS
            assert msg == ""
        elif script.__name == 'error':
            assert res == RES_ERROR

    stats, failures = run_scripts(coll, 'test')

    assert len(failures) == len(coll)
    assert failures[0][0] == 'fail'
    assert failures[0][2] == 'Fail'

    for test_name in ['skip', 'pass', 'fail']:
        for test_result in ['skip', 'pass', 'fail']:
            if test_name == test_result:
                assert stats['test.lu'][test_name][test_result] == len(coll)
            else:
                assert stats['test.lu'][test_name][test_result] == 0
        assert 'error' not in stats['test.lu'][test_name]
        assert stats['test.lu']['error'][test_name] == 0

    assert stats['test.lu']['error']['error'] != ""
