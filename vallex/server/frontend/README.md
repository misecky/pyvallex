# Vallex Search Frontend

The Frontend is coded using the [Vue.js](https://vuejs.org/) framework
and UI components from the [Vuetify](https://vuetifyjs.com/en/) library. The directory layout is as follows:

  - `node_modules`: contains external libraries, installed using [yarn](https://yarnpkg.com/en/)
  - `dist`: compiled & minified frontend code (this contains the static main page (index.html) and the necessary javascript, image and style files files, which are served by the python server
  - `src`: the actual source code

The entry-point for the frontend application is located in [`src/App.vue`](./src/App.vue) (technically speaking the main entry point is
in `src/main.js`, but the interesting stuff happens in `App.vue`).
This file contains the main Vue component (see [`src/components/README.md`](src/components/README.md) for more info on components.).

Currently, the frontend expects the backend (`vallex-cli web`)
to run on [localhost:8080](http://localhost:8080) (this can be
changed in the `.env.development`, `.env.client` and `.env.server` files). There are two ways to serve the frontend
files. First, `vallex-cli` (without arguments) starts the backend and automatically serves the compiled frontend on [localhost:8800](http://localhost:8800). However, for
development it is easier to serve the frontend using
```
$ yarn run serve
```
which will serve the frontend on [localhost:8081](http://localhost:8081) and will recompile the source code and reload the page whenever you make any changes.
In order to serve the frontend files in this way while simultaneously running the backend, use the script [quality/debug-frontend.sh](../../../quality/debug-frontend.sh).


### Project setup
First make sure you have [NodeJS](https://nodejs.org/en/) installed
and then run:

```
$ npm install -g yarn
$ yarn install
```

### Compiles and hot-reloads for development
```
$ yarn run serve
```

### Compiles and minifies for production
```
$ yarn run build
```

### Run your tests
```
$ yarn run test
```

### Lints and fixes files
```
$ yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
