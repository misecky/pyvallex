import Vue from "vue"
import "./plugins/vuetify"
import App from "./App.vue"

import "./scss/icons.scss"
import "vue-material-design-icons/styles.css"
import "./scss/utils.scss"
import "./scss/print.scss"

import config from "./config.js"

import axios from "axios"

import * as Sentry from "@sentry/browser"

Vue.config.productionTip = false

if (process.env.VUE_APP_ENABLE_PERF) {
    Vue.config.performance = true
}

if (process.env.VUE_APP_ENABLE_DEV) {
    Vue.config.devtools = true
}

new Vue({
    render: (h) => h(App),
}).$mount("#app")

if (!process.env.VUE_APP_DISABLE_SENTRY) {
    Sentry.init({
        dsn: "https://6511a8ea7b8c4750a2b573af1f4e2ad2@sentry.io/1405151",
        integrations: [
            new Sentry.Integrations.Vue({
                Vue,
                attachProps: true,
            }),
            new Sentry.Integrations.ExtraErrorData({
                depth: 3, // default value
            })
        ],
        beforeSend: (event) => {
            axios.post(
                config.server + "/api/rpc/log",
                {
                    area: 'sentry',
                    level: 'ERROR',
                    message: event,
                },
            );
            return event;
        },
    })
}
