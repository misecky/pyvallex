""" Session

    https://gitlab.com/Verner/pyvallex/-/issues/119

    This file provides the implementation of the Session class which holds
    session id, session data and session expiration.
    Session is created after logging into the web app.
"""

import secrets
import hashlib
from datetime import datetime
from datetime import timedelta

from vallex.json_utils import loads, dumps
from vallex.term import *
import logging
from vallex.log import log
from vallex.vendor import bottle


class Session:
    SESSION_COOKIE_NAME = 'session_id'

    def __init__(self, store, session_id=None):
        self.session_id = session_id or secrets.token_urlsafe(64)
        self._store = store
        self._new_session = session_id is None
        if self._new_session:
            self._data = {'logged_in': None}
            log('session:__init__', logging.DEBUG, "initialized", self.session_id, self._data)
        else:
            try:
                with (self._store.execute_sql("SELECT data FROM sessions WHERE cookie = ? AND (? < expires)",
                                              (self.session_id, datetime.now()))) as sessions:
                    data = sessions.fetchall()
                    assert len(data) == 1, f"There are {len(data)} sessions under the same id in the DB! Expected a single one."
                    self._data = loads(data[0]['data'])
                    log('session:__init__', logging.DEBUG, "restored", self.session_id, self._data)

            except Exception as ex:
                log('session:__init__', logging.ERROR, "loading", self.session_id, ":", ex)
                self._data = {'logged_in': None}
                self._new_session = True
                self.session_id = secrets.token_urlsafe(64)

    def close(self):
        """Saves the data to the db and closes the session"""
        if self._new_session:
            try:
                self._store.run_sql("INSERT INTO sessions (cookie, data, expires) VALUES (?, ?, ?)",
                                    (self.session_id, dumps(self._data), datetime.now() + timedelta(days=1)))
                log('session:close', logging.DEBUG, "SQL insert", self.session_id, self._data)
                return True
            except Exception as ex:
                log('session:close', logging.ERROR, "SQL insert", self.session_id, ":", ex)
                return False
        else:
            try:
                self._store.run_sql("UPDATE sessions SET data = ?, expires = ? WHERE cookie = ?",
                                    (dumps(self._data), datetime.now() + timedelta(days=1), self.session_id))
                log('session:close', logging.DEBUG, "SQL update", self.session_id, self._data)
                return True
            except Exception as ex:
                log('session:close', logging.ERROR, "SQL update", self.session_id, ":", ex)
                return False

    def login(self, name: str, password: str):
        """
            Log in to session
            checks a POST-ed user/password pair and, if correct, sets a logged_in flag in the session to true
        """
        with self._store.execute_sql("SELECT * FROM users WHERE name = ?", (name,)) as users:
            user = users.fetchall()
            if not user:
                return False, f"Authentication failed!"  # User {name} does not exists!"
            if len(user) > 1:
                # TODO unexpected error -- osetrit dale
                return False, f"Authentication failed!"  # There exists multiple ({len(user)}) users of the same name in the DB!"

            name, salt, password_hashed = user[0][0], user[0][1], user[0][2]
            login_password_hashed = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)
            if password_hashed == login_password_hashed:
                self._data['logged_in'] = name
                return True, f"User {name} is authenticated"
            else:
                return False, "Authentication failed!"  # Wrong password!"

    def logout(self):
        """Log out from session"""
        if self._data['logged_in'] is None:
            msg = "No user is logged in"
        else:
            msg = f"User {self._data['logged_in']} logged out"
        self._data['logged_in'] = None
        return True, msg

    def isLogged_in(self):
        return True if self._data['logged_in'] else False
