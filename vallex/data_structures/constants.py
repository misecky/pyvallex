import re

VALID_ATTRIBUTE_NAMES = [
    'alternations', 'class', 'control', 'conv',
    'derived', 'derivedLUs', 'derivedFrom',  # TODO: test that 'derived' is not used - it should be one of the other two
    'diat',
    'example', 'examplerich', 'example1', 'example2', 'example3', 'example4', 'example5', 'example6', 'example7',
    'full',
    'instig', 'instig1', 'instig2', 'instig3', 'instig4', 'instig5', 'instig6', 'instig7',
    'limit',
    'full', 'lvc', 'lvc1', 'lvc2', 'lvc3', 'lvc4', 'lvc5', 'lvc6', 'lvc7', 'lvcN', 'lvcV',
    'map', 'map1', 'map2', 'map3', 'map4', 'map5', 'map6', 'map7',
    'multiple', 'negation', 'note', 'otherforms', 'pdt-vallex',
    'recipr', 'recipr1', 'recipr2', 'recipr3',
    'reciprevent', 'reciprevent1', 'reciprevent2', 'reciprevent3',
    'reciprtype',
    'reciprverb', 'reciprverb1', 'reciprverb2', 'reciprverb3',
    'reflex', 'reflex1', 'reflex2', 'reflex3', 'reflexverb',
    'semcategory', 'shortform', 'specval', 'status', 'synon', 'split', 'type', 'use', 'valdiff', 'restriction',
    # and for Polish data also:
    'ref'
]
"""A list of all valid attribute names"""
ATTRIB_REGEX = re.compile(r'-(?P<value>[\w-]+):[ \t]*')

FUNCTORS = [
    'ACMP', 'ACT', 'ADDR', 'ADVS', 'AIM', 'APP', 'APPS', 'ATT', 'AUTH', 'BEN', 'CAUS',
    'CNCS', 'COMPL', 'COND', 'CONFR', 'CONJ', 'CPHR', 'CPR', 'CRIT', 'CSQ', 'CTERF',
    'DENOM', 'DES', 'DIFF', 'DIR', 'DIR1', 'DIR2', 'DIR3', 'DISJ', 'DPHR', 'EFF',
    'ETHD', 'EXT', 'FPHR', 'GRAD', 'HER', 'ID', 'INTF', 'INTT', 'LOC', 'MANN', 'MAT',
    'MEANS', 'MOD', 'NA', 'NONE', 'NORM', 'OBST', 'ORIG', 'PAR', 'PARTL', 'PAT', 'PN',
    'PREC', 'PRED', 'RCMP', 'REAS', 'REG', 'RESL', 'RESTR', 'RHEM', 'RSTR', 'SENT',
    'SUBS', 'TFHL', 'TFRWH', 'THL', 'THO', 'TOWH', 'TPAR', 'TSIN', 'TTILL', 'TWHEN',
    'VOC', 'VOCAT', 'EMPTY'
]
"""The list of valid functors"""
FUNCTOR_RGX_SRC = r'('+r'|'.join(['[-+=]?'+f for f in FUNCTORS])+r')'
"""Regexp matching valid functors"""

ACTANT_FUNCTORS = ['ACT', 'ADDR', 'PAT', 'EFF', 'ORIG']
"Functors that are considered 'ACTANTS'"
ACTANTS_AND_CPHR = ACTANT_FUNCTORS + ['CPHR']

VALLEX_ASPECTS = ['pf', 'impf', 'iter', 'no-aspect', 'biasp']
VALLEX_ASPECTS_NUMBERED = ['pf', 'pf1', 'pf2', 'pf3', 'impf', 'impf1', 'impf2', 'iter', 'iter1', 'iter2', 'no-aspect', 'no-aspect1', 'no-aspect2', 'biasp']
PDTVALLEX_ASPECTS = ['V', 'Nx', 'N', 'A', 'D']
VALLEX_ASPECTS_REGEX = re.compile(r'(?P<value>'+r'|'.join(VALLEX_ASPECTS_NUMBERED)+r'):\s')
ASPECTS_REGEX = re.compile(r'(?P<value>'+r'|'.join(VALLEX_ASPECTS_NUMBERED+PDTVALLEX_ASPECTS)+r'):\s')
