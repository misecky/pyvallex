""" A transform for adding/recomputing the valdiff attribute in noun data




    ./vallex-cli --no-sort --output-format txt -o ../aktualni_data/data-txt/ scripts transform vallex/scripts/transforms/add_valdiff.py

    Note that the file with the verb data (v-vallex.txt) is exported empty.
"""

import logging
import sys
import os  # for better debugging

from vallex import Frame, Valdiff
from vallex.log import log
from vallex.scripts import changes, requires, TestDoesNotApply, TestFailed


@changes('valdiff')
@requires('lumap')
def transform_lu_add_valdiff(lu, lumap):
    log("add_valdiff", logging.DEBUG, "the original frame of", lu._id, "before computing valdiff is ", str(lu.frame))
    if 'pos' not in lu.dynamic_attribs or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective'):
        raise TestDoesNotApply
    if 'derivedFrom' not in lu.attribs or lu.attribs['derivedFrom']._data['ids'] == []:
        raise TestDoesNotApply
    try:
        verb = lumap[lu.attribs['derivedFrom']._data['ids'][0]._id]
        log("add_valdiff", logging.DEBUG, "first derivedFrom value of ", lu._id, ": ", verb._id)
    except Exception as ex:
        log("add_valdiff", logging.WARN, "error adding valdiff to ", lu, " with derivedFrom=", str(lu.attribs['derivedFrom']._data), " failed with: ", ex)
        raise TestFailed

    try:
        if lu._id in ['blu-n-velení-1', 'blu-n-velení-3', 'blu-n-vyučování-vyučení-1', 'blu-n-výuka-1']:
            systemic_changes = {x: Valdiff.SYSTEMIC_CHANGES_VERB_NOUN[x] for x in Valdiff.SYSTEMIC_CHANGES_VERB_NOUN if x != '3'}   # in those two LUs, 3>2,pos should not be treated as a typical change
        elif lu.dynamic_attribs['pos']._data == 'adjective':
            systemic_changes = Valdiff.SYSTEMIC_CHANGES_VERB_ADJECTIVE
        else:
            systemic_changes = Valdiff.SYSTEMIC_CHANGES_VERB_NOUN
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log("add_valdiff", logging.ERROR, "computing changeset for ", lu._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    try:
        ret = Valdiff.diff(verb.frame, lu.frame, systemic_changes=systemic_changes)
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        log("add_valdiff", logging.ERROR, "computing valdiff for ", lu._id, " and the first source verb ", verb._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    if len(lu.attribs['derivedFrom']._data['ids']) > 1:
        for verb_ref in lu.attribs['derivedFrom']._data['ids'][1:]:
            log("add_valdiff", logging.DEBUG, "later derivedFrom value of ", lu._id, ": ", verb_ref._id)
            try:
                verb = lumap[verb_ref._id]
            except Exception as ex:
                log("add_valdiff", logging.WARN, "error adding later value of derivedFrom to valdiff of ", lu, " with derivedFrom=", str(lu.attribs['derivedFrom']._data), " failed with: ", ex)
                raise TestFailed

            try:
                valdiff = Valdiff.diff(verb.frame, lu.frame, systemic_changes=systemic_changes)

                for slot in valdiff._data:
                    list_slots = [i for i, oldslot in enumerate(ret._data) if oldslot.functor == slot.functor]
                    if slot.spec == "-" and len(list_slots) == 0:
                        ret._data.append(slot)
                    elif len(list_slots) != 1:
                        log("add_valdiff", logging.ERROR, "there is not a unique slot with the", slot.functor, "functor:", ' '.join([ret._data[i] for i in list_slots]))
                    else:
                        list_slots = [i for i, oldslot in enumerate(ret._data) if oldslot.functor == slot.functor]
                        if len(list_slots) != 1:
                            log("add_valdiff", logging.ERROR, "there is not a unique slot with the", slot.functor, "functor: ", ' '.join([ret._data[i] for i in list_slots]))
                        else:
                            old_index = list_slots[0]
                            if slot.spec == '=':
                                ret._data[old_index].spec = '='  # even if it was added relative to the first verb

                            for form in slot.forms_eq:  # it is in the noun frame -> it was either eq or added
                                if form in ret._data[old_index].forms_add:
                                    log("add_valdiff", logging.DEBUG, "should treat form", form, "as equal, not added")
                                    ret._data[old_index].forms_eq.append(form)
                                    # ret._data[old_index].forms_add.remove(form) would remove form also from the frame of the lu, because Valdiff.diff does not contain a deep copy of the elements...
                                    ret._data[old_index].forms_add = [x for x in ret._data[old_index].forms_add if x != form]
                            for form in slot.forms_del:  # it is not in the noun frame -> it either was not in valdiff or is already in forms_del
                                if not form in ret._data[old_index].forms_del:
                                    log("add_valdiff", logging.DEBUG, "should treat form", form, "as deleted")
                                    ret._data[old_index].forms_del.append(form)
                            # for form in slot.forms_add:
                                    # we do not need to do anything -- the form is either already added, or even eq
                            if slot.oblig_source == slot.oblig_derived:  # we prefer no change in obligatoriness
                                ret._data[old_index].oblig_source = slot.oblig_source

            except Exception as ex:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                log("add_valdiff", logging.ERROR, "computing valdiff between ", lu._id, " and additional source verb ", verb._id, " resulted in exception of ", exc_type, "in file", fname, "at line", exc_tb.tb_lineno)

    lu.attribs[ret.name] = ret
    log("add_valdiff", logging.DEBUG, "exit transform_lu_add_valdiff, unit", lu._id, "has frame", str(lu.frame), "and valdiff", lu.attribs['valdiff'])
