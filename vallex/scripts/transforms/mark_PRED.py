""" A transform for marking the lexeme lemmas in the examplerich attribute






"""

import logging
import sys
import os        # for better debugging
import requests  # type: ignore  # for calling an external api
import time      # for waiting between submitting requests
import re

from vallex import Lemma
from vallex.data_structures import Text
from vallex.log import log
from vallex.scripts import changes, requires, TestDoesNotApply, TestFailed
from typing import List


def call_morphodita(text: str, splitSentences=False) -> list:
    """
        Calls an external parser on a piece of text.
        Returns
          - if splitSentences=False:
              a list of dicts of the form
              {"token":"Děti","lemma":"dítě","tag":"NN","space":" "}.
          - if splitSentences=True:
              a list of lists of the form given above,
              one for each sentence
    """
    url = 'https://lindat.mff.cuni.cz/services/morphodita/api/tag'
    parameters = {
        "data": text,
        "output": "json",
        "model": "czech-morfflex-pdt-161115",
        "convert_tagset": "strip_lemma_id"
    }
    try:
        time.sleep(1/14)  # there is a limit of 15 requests per second for the morphodita API
        response = requests.get(url, parameters)
        result = response.json()['result']
        if splitSentences:
            return result
        else:
            return [word for sentence in result for word in sentence]
    except Exception as ex:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        log("mark_PRED", logging.ERROR, "attempt to call an external api resulted in exception of ", exc_type, ":", ex)
        return [None]


def markLemmaInParsedText(parsedText: list, testlemmas: list, testreflexives: list) -> str:
    """
        Gets a parsedText in the form of a list of dicts of the form
            {"token":"Děti","lemma":"dítě","tag":"NNFP1-----A----","space":" "}.
        and returns a string consisting of concatenated tokens and spaces,
        with tokens whose lemma appears in testlemmas marked as ".PRED"
        or ".PRED_NEG" (for the negated form)
        and "se" or "si" marked as ".PRED_SE/.PRED_SI" if it possibly could be
        part of the lemma.
    """
    text = ''
    for word in parsedText:
        text += word['token']
        if word['token'] in testreflexives:
            text += '.PRED_' + word['token'].upper()  # se/si which may be part of the lemma
        elif word['lemma'] in testlemmas:
            if word['tag'][10] == 'N':  # the 11th position is negation
                text += '.PRED_NEG'
            else:
                text += '.PRED'
        elif 'ne'+word['lemma'] in testlemmas and word['tag'][10] == 'N':
            text += '.PRED'
        if 'space' in word.keys():
            text += word['space']
    return text


known_errors = {
    # A dictionary linking lu ids to changes that need to be performed
    # at the end of transform_lu_mark_PRED()
    # to revert changes that are known to be erroneous.
    'blu-a-jistý-si-2': ['Tam si.PRED_SI m', 'Tam si m'],
    'blu-a-spokojený-1': ['spokojení', 'spokojení.PRED'],  # dva různé příklady
}


def iterateOverExamples(examples: list, testlemmas: list, testreflexives: list, lu_id: str) -> None:
    for i in range(len(examples)):
        example = str(examples[i]).replace('.PRED_NEG', '').replace('.PRED_SE', '').replace('.PRED_SI', '').replace('.PRED', '')
        parsed = call_morphodita(example, splitSentences=False)
        adjustedExample = markLemmaInParsedText(parsed, testlemmas, testreflexives)
        if lu_id in known_errors.keys():
            adjustedExample = adjustedExample.replace(known_errors[lu_id][0], known_errors[lu_id][1])
        examples[i] = Text(txt=adjustedExample, comments=examples[i].comments)


@changes('examplerich')
def transform_lu_mark_PRED(lu):
    if ('pos' not in lu.dynamic_attribs
            or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective')):
        log("mark_PRED", logging.WARNING, "mark_PRED expects only nouns and adjectives:", lu._id)
        raise TestDoesNotApply

    try:
        examplerich = lu.attribs['examplerich']._data
    except Exception as ex:
        log("mark_PRED", logging.DEBUG, "can only be applied if the LU contains an examplerich attribute:", lu._id)
        raise TestDoesNotApply
    log("mark_PRED", logging.DEBUG, "the original value of the examplerich attribute of", lu._id, " is:\n", str(lu.attribs['examplerich']))

    try:
        testlemmas = lu.lemma.lemma_set(discern_homo=False, noiter=False, treat_reflex='remove')
    except Exception as ex:
        log("mark_PRED", logging.ERROR, "computation of lemmas failed for", lu._id)
    try:
        testreflexives = lu.lemma.reflexives(include_optional=True)
    except Exception as ex:
        log("mark_PRED", logging.ERROR, "computation of reflexives failed for", lu._id)

    for exs in examplerich.values():
        if type(exs) == list:
            exs = [exs]
        else:
            exs = exs.values()
        for examples in exs:
            iterateOverExamples(examples, testlemmas, testreflexives, lu._id)

    log("mark_PRED", logging.DEBUG, "the resulting value of the examplerich attribute of", lu._id, " is:\n ", str(lu.attribs['examplerich']))
