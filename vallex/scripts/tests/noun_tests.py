from vallex.scripts import requires, TestDoesNotApply, TestFailed

import re


@requires('lumap')
def test_lu_derived_references(lu, lumap):
    """
        The derived attribute should point to an existing lu.
    """
    failures = []
    derived_varianty = [k for k in lu.attribs.keys() if k.startswith('derived')]
    if not derived_varianty:
        raise TestDoesNotApply
    applies = False
    for attrib in derived_varianty:
        if not isinstance(lu.attribs[attrib]._data, dict):
            continue
        refs = lu.attribs[attrib]._data['ids']
        if refs:
            applies = True
            for ref in refs:
                if not ref._id.startswith('@') and ref._id not in lumap and ref != 'Vallex-no' and ref != 'no':
                    failures.append(str(ref._id))
        else:
            continue
    if failures:
        raise TestFailed("The following references not found in db: "+','.join(failures))
    if not applies:
        raise TestDoesNotApply


PRED_REGEX = re.compile('[.]PRED(?!_(NEG|S[EI]))')
PRED_NEG_REGEX = re.compile('[.]PRED_NEG')
PRED_SE_SI_REGEX = re.compile('[.]PRED_S[EI]')

mark_PRED_exceptions_none = {
    # set of examples that may contain no PRED
    "TODO",
}
mark_PRED_exceptions_multiple = {
    # set of examples that contain multiple PRED
    "Podstatnější než její očividná krása však byla hrdost, která z ní vyzařovala, hrdé.PRED držení těla, hrdý.PRED výraz v úzké tváři.",
    "Hrůza.PRED neusnout.PAT a hrůza.PRED ze smrti.PAT je stejná.",
    "Sejme je z nich, až sám bude mocen.PRED dostát.PAT svým závazkům, a toho.PAT bude mocen.PRED, až zas sám uzná za vhodné, jako tomu je v případě bytů.",
    "A stále běhá po světě a sužuje chudý lid a mocné.PRED vlády mocných.PRED zemí si s ní neví rady.",
    "vidí, jak se Druhý netrpělivě dívá na hodinky, navyklost.PRED gesta.PAT v navyklosti.PRED shonu.PAT",
    "Bylo ale nutné, aby moje.ACT nedočkavost.PRED, až to celé skončí.PAT, vypadala jako nedočkavost.PRED, kdy už to začne.PAT.",
    "Za zhoubnou nenávistí.PRED vůči Židům.PAT je třeba vidět nadčasovou a celosvětově rozšířenou nenávist.PRED nevzdělané a líné chátry.ACT vůči lidem.PAT podnikavým a kultivovaným.",
    "Odpovědnost.PRED za porušení.PAT této povinnosti ... je přitom nejen politická (odpovědnost.PRED voličům.ADDR), ale samozřejmě i právní.",
    "Dnes už je to otázka odvahy.PRED. Odvahy.PRED lékaře.ACT, aby zachránil.PAT život člověka.",
    "Na zajímavé výlety za technikou stačí jen trochu odvahy.PRED. Odvahy.PRED na překonání.PAT předsudků.",
    "Výjimka z povinnosti.PRED mlčenlivosti.PAT advokáta se však týká pouze povinnosti.PRED překazit.PAT trestný čin podle § 167 trestního zákona.",
    "nahrazuje pracovní povolení.PRED a povolení.PRED k dlouhodobému pobytu.PAT",
    "Lagrangeův bod nad odvrácenou stranou je nejvzdálenějším libračním centrem v soustavě Země - Měsíc, místem, kde přitažlivost.PRED lodi.PAT k Zemi.DIR3 je přesně vyvážena její.PAT přitažlivostí.PRED k Měsíci.DIR3, což udržuje loď v klidu relativně k těmto dvěma tělesům.",
    "přestává hrát kapitalistické vlastnictví a z něj vyplývající jednotné velení.PRED kapitalisty.ACT tu roli, kterou hrálo např. v prosté kooperaci, manufakturní nebo strojové výrobě. Naopak právě toto velení.PRED kapitálu.ACT nad prací.PAT ztělesněné v rozbujelých manažerských strukturách se stává významnou překážkou využití sil všeobecné práce",
    "Dne 5. 10. 2009 byla na podatelnu Obecního úřadu doručena výpověď.PRED nájemní smlouvy.CPHR současného nájemce.ACT prodejny potravin a smíšeného zboží, paní Ireny Jirkové.ACT, bytem Lesní Hluboké č. p. 68., a to k 31. 12. 2009. Zastupitelstvo obce tuto její.ACT výpověď.PRED akceptovalo.",
    "Jeho.ACT výtka.PRED je vlastně táž jako Durychova.ACT výtka.PRED Čapkovi.ADDR: zpovrchnění",
    "Mé.PAT vyznamenání.PRED od prezidenta.ACT republiky je bezesporu i vyznamenáním.PRED jihomoravských vinařů.PAT, kteří udělali velký kus poctivé práce",
    "soud informuje a vyslechne toho, kdo je schopen zájmy.PRED dítěte.ACT ochránit a jehož.ACT zájmy.PRED nejsou v rozporu se zájmy.PRED dítěte.ACT",
    "Přidáme-li burky, zákaz.PRED žen.ADDR řídit.PAT auta, případně zákaz.PRED vycházet.PAT bez doprovodu mužského příbuzného, de facto hodnotíme islám podobně, jako kdybychom pravidla mormonů vztahovali na celé křesťanstvo.",
    "Různé způsoby zasouvání dveří do zdi nabízejí tato řešení: jednokřídlé dveře zasouvací.PRED do pouzdra.DIR3, dvoje jednokřídlé dveře zasouvací.PRED proti sobě.DIR3, dvoje jednokřídlé dveře zasouvací.PRED k sobě.DIR3 a dvoukřídlé dveře zasouvací.PRED za sebe.DIR3.",
    "Je tedy religionista, zaujatý.PRED pro co nejvyšší míru.PAT objetivity, zaujatý.PRED snahou.PAT postihnout co nejpřiměřeněji, jak studovaný jev sám sebe explikuje",
    "Jejich.ACT zdání.PRED způsobilosti.PAT, které zachovávali, bylo právě tím: zdáním.PRED.",
    "Základní nezbytnou vlastností otopného systému je v tomto případě jeho schopnost v maximální míře akceptovat solární zisky.PRED, stejně tak jako eventuální zisky.PRED od vnitřních zdrojů.ORIG tepla.",
    "on seděl s dětmi a ukazoval jim známé.PRED stavby, které míjeli cestou. Známé.PRED pro něho.ACT, ale nikoli jim.ACT.",
    "Stejně jako Hus byl zoufalý.PRED, jak vrchnost kupčila.PAT s odpustky, tak my jsme zoufalí.PRED z korupce.PAT na nejvyšších místech.",
}
mark_PRED_exceptions_NEG = {
    # set of examples where we expect a PRED_NEG mark
    "V nižších soutěžích si musí dát pozor na uznávání.PRED či neuznávání.PRED_NEG gólů.PAT",
    "Rodiče dostávají informace o povinném.PRED i doporučovaném nepovinném.PRED_NEG očkování",
    "Bohužel setrvačností často mizí pozornost.PRED vůči životu.PAT partnera a do vztahu se vloudí pohodlnost, pasivita, nepozornost.PRED_NEG až lhostejnost vůči potřebám toho druhého.",
}
mark_PRED_exceptions_SE_SI = {
    # set of examples with multiple PRED_SE / PRED_SI
    "TODO",
}


def test_lu_mark_PRED(lu):
    """
        Tests that each line in the examplerich attribute contains exactly one
        instance of the .PRED mark,
        and at most one instance of .PRED_SE or .PRED_SI,
        and no .PRED_NEG unless the lu is known to have one.
    """
    if ('pos' not in lu.dynamic_attribs
            or not lu.dynamic_attribs['pos']._data in ('stem noun', 'root noun', 'adjective')):
        raise TestDoesNotApply

    try:
        examplerich = lu.attribs['examplerich']._data
    except Exception as ex:
        raise TestDoesNotApply

    failures = []
    for key in examplerich.keys():
        for example in [str(ex) for ex in examplerich[key]]:
            examplePRED = PRED_REGEX.findall(example)
            examplePRED_NEG = PRED_NEG_REGEX.findall(example)
            examplePRED_SE_SI = PRED_SE_SI_REGEX.findall(example)
            if not examplePRED and example not in mark_PRED_exceptions_none:
                failures.append('No PRED in "' + example + '".')
            elif len(examplePRED) > 1 and example not in mark_PRED_exceptions_multiple:
                failures.append('Multiple occurrences of PRED in "' + example + '".')
            elif examplePRED_SE_SI and len(examplePRED_SE_SI) != len(examplePRED) and example not in mark_PRED_exceptions_SE_SI:
                failures.append('Number of PRED_SE/PRED_SI does not equal the number of PRED in "' + example + '".')
            elif examplePRED_NEG and example not in mark_PRED_exceptions_NEG:
                failures.append('PRED_NEG not expected in "' + example + '".')
    if failures:
        raise TestFailed("\n".join(failures))
