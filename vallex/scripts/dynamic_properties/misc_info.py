import logging
from vallex.log import log
import sys
import os  # for better debugging

import re

from vallex import Attrib

PDTVALLEX_POS_MAPPING = {
    'V':  'verb',
    'N':  'stem noun',
    'Nx': 'root noun',
    'A':  'adjective',
    'D':  'adverb'
}


def compute_pos(lu):
    attr = Attrib('pos', dynamic=True, help='Detailed part of speech (verb / stem noun / root noun / adjective / unknown)')
    attr._data = 'unknown'
    if 'blu-v' in lu._id:
        attr._data = 'verb'
    if 'blu-a' in lu._id:
        attr._data = 'adjective'
    elif 'blu-n' in lu._id:
        attr._data = 'stem noun'
        for var in ['', '1', '2', '3', '4']:
            if 'no-aspect'+var in lu.lemma._data.keys():
                attr._data = 'root noun'
                break
    elif 'v-w' in lu._id:
          # next(iter(dictionary)) gets a key from that dict
          # we rely on the fact that in pdt-vallex, the lu has a single lemma, so there is only one key
        attr._data = PDTVALLEX_POS_MAPPING[next(iter(lu.lemma._data))]
    lu.dynamic_attribs[attr.name] = attr


REFLEXIVE_RGX = re.compile(r'.*\s+\bs[ei]\d?\b\s*$')
"""A regexp for recognizing reflexive lemmas."""
OPT_REFLEXIVE_RGX = re.compile(r'.*\s+\(s[ei]\d?\)\s*$')
"""A regexp for recognizing optionally reflexive lemmas."""


def compute_isReflexive(lu):
    attr = Attrib('isReflexive', dynamic=True, help='Is the lemma reflexive (always/optionally/never)?')
    if sum(1 for val in lu.lemma._data.values() if REFLEXIVE_RGX.match(val)) > 0:
        attr._data = 'always'
    elif sum(1 for val in lu.lemma._data.values() if OPT_REFLEXIVE_RGX.match(val)) > 0:
        attr._data = 'optionally'
    else:
        attr._data = 'never'
    lu.dynamic_attribs[attr.name] = attr
