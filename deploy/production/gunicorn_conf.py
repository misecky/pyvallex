# import multiprocessing
proc_name = 'pyvallex'
pythonpath = '/home/www/vallex/code'
# workers = multiprocessing.cpu_count() * 2 + 1
workers = 4
timeout = 120
bind = 'unix:/home/www/vallex/run/gunicorn.sock'
