#!/bin/bash

# Check differences between the source file and the parsed file (collection) stored in the db
# Optional argument --diff or --gui shows the diff (in txt form or in a gui program kdiff3)
SRC=$1
OUT=`mktemp ${SRC}.XXX.out`
SRCCOMP=`mktemp ${SRC}.XXX.srccomp`
OUTCOMP=`mktemp ${SRC}.XXX.outcomp`

function simplify {
  cat $1 | \
  sed -e 's/[ \t;]/\n/g' -e 's/[„“‚‘]/"/g' -e "s/'//g" -e 's/[#|%,{}]//g' | \
  sed -e "s/\[//g" -e "s/\]//g" | \
  sed '/^$/d'
}
# Prepare collection data for comparison
./vallex-cli --no-sort -i $SRC grep > $OUT
simplify $SRC > $SRCCOMP
simplify $OUT > $OUTCOMP

diff_abs=$(diff --ignore-all-space --ignore-blank-lines $SRCCOMP $OUTCOMP | wc -l)
sz_in=$(cat $SRCCOMP | wc -l)
echo -e "\n$diff_abs different 'words' identified out of the $sz_in 'words' in $1"
echo -e "relative difference: " | tr -d "\n"
echo -e "scale=3;$diff_abs/$sz_in" | bc
echo -e "\n"

if [ "g$2" == "g--gui" ]; then
    kdiff3 $SRC $OUT & kdiff3 $SRCCOMP $OUTCOMP
elif [ "g$2" == "g--diff" ]; then
    echo "DIFF of source file and collection parsed by pyvallex:"
    diff -ybw --suppress-common-lines $SRC $OUT
    echo
    echo "======================================================"
    echo "DIFF (WORDS ONLY) of source file and collection parsed by pyvallex:"
    diff -y --ignore-all-space --ignore-blank-lines --ignore-matching-lines='(.*idiom:.*)|(.*.*+i .*)' $SRCCOMP $OUTCOMP 
    echo -e "\n"
fi;

rm $OUT $SRCCOMP $OUTCOMP
